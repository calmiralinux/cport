#!/usr/bin/python3
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import argparse
import os
import sys

import cport_cli.args_parser as ccap  # cport CLI Arguments Parser - CCAP
import libcport.core as cc
import libcport.datatypes as cd
from libcport.constants import DATABASE_FILE, DATABASE_USER_FILE, LOCAL_LIB_DIR, VERSION

import cport_cli.info as cci
# import cport_cli.upgrade as ccU

VER_MSG = f"""cport {VERSION} - the port manager for Calmira GNU/Linux-libre
Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

This is free software; you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"""

parser = argparse.ArgumentParser()
subparser = parser.add_subparsers()

parser.add_argument(
    "--install", "-i", nargs="+", help="Скачать, распаковать, собрать и установить порт в систему"
)

parser.add_argument(
    "--remove", "-r", nargs="+", help="Удалить порт из системы"
)

parser.add_argument(
    "--info", "-I", dest="info", nargs="+",
    help="Просмотреть информацию о программном обеспечении в портах"
)

parser.add_argument(
    "--info-json", dest="info_json", nargs="+",
    help="Просмотреть информацию о ПО в портах в формате JSON"
)

parser.add_argument(
    "--update", "-u", dest="update", action="store_true",
    help="Проверить обновления системы портов"
)

parser.add_argument(
    "--upgrade", "-U", dest="upgrade", nargs="+",
    help="Обновить указанное пользователем программное обеспечение"
)

parser.add_argument(
    "--get-flist", dest="get_flist",
    help="Вывести список файлов указанного порта"
)

list_parser = subparser.add_parser('list', help='list of ports')
list_parser.add_argument(
    '-a', '--all', dest='list_all', action='store_true',
    help="Просмотреть список всех портов"
)
list_parser.add_argument(
    '-i', '--installed', dest='list_installed', action='store_true',
    help="Просмотреть список установленных портов"
)
list_parser.add_argument(
    '-I', '--not-installed', dest='list_not_installed', action='store_true',
    help="Просмотреть список НЕ установленных портов"
)
list_parser.add_argument(
    '-b', '--broken', dest='list_broken', action='store_true',
    help="Просмотреть список битых портов"
)
list_parser.add_argument(
    '-j', '--json', dest='list_json', action='store_true',
    help="Вывести информацию о портах в формате JSON"
)
list_parser.set_defaults(func=ccap.ports_list)

deps_parser = subparser.add_parser('deps', help='getting a list of packages that have the required one as a dependency')
deps_parser.add_argument(
    "--all", "-a", dest="deps_all", help="all types of dependencies",
    action="store_true"
)
deps_parser.add_argument(
    "--required", "-r", dest="required_deps", help="required dependencies",
    action="store_true"
)
deps_parser.add_argument(
    "--recommend", "-R", dest="recommend_deps", help="recommend dependencies",
    action="store_true"
)
deps_parser.add_argument(
    "--optional", "-o", dest="optional_deps", help="optional dependencies",
    action="store_true"
)
deps_parser.add_argument(
    "--name", "-n", dest="dest_name", required=True, type=str,
    help="set the name of port"
)
deps_parser.add_argument(
    "--json", "-j", dest="deps_json", action="store_true"
)
deps_parser.set_defaults(func=ccap.port_deps)

check_parser = subparser.add_parser('check', help='check all ports')
check_parser.add_argument(
    '--is-exist', dest='check_exist', help="Проверка наличия порта в СП"
)
check_parser.add_argument(
    '--is-installed', dest='check_installed', help="Проверка наличия порта в БД"
)
check_parser.add_argument(
    '--check-ports', dest='check_all', action='store_true',
    help="Проверка наличия зависимостей всех портов в СП"
)
check_parser.add_argument(
    '--ports-linter', dest='plint', action='store_true',
    help="Проверка портов на корректность"
)
check_parser.set_defaults(func=cci.ports_check)

parser.add_argument(
    "--yes", "-y", dest="confirm_proc", action="store_true",
    help="Отвечать утвердительно на выполнение всех операций"
)

parser.add_argument(
    "--ignore-db", dest="ignore_db", type=bool, default=False,
    help="Игнорировать проверку базы данных перед установкой или удалением порта"
)

parser.add_argument(
    "--skip-check", "-C", dest="skip_check", default=False,
    action="store_true", help="Не проверять наличие или отсутствие файлов порта после установки или удаления"
)

parser.add_argument(
    "--rebuild-db", dest="rebuild_db", action="store_true",
    help="Обновить базу данных cport"
)

parser.add_argument(
    "--wipe-cache", "-W", dest="wipe_cache", action="store_true",
    help="Удалить все файлы, которые находятся в кеше"
)

parser.add_argument(
    "-v", "--version", action="store_true",
    help="Print the cport version"
)

args = parser.parse_args()

if not args:
    parser.print_usage()
    exit(1)

skip_checks = False
if args.skip_check:
    skip_checks = True

ignoredb = False
if args.ignore_db:
    ignoredb = True

confirm = False
if args.confirm_proc:
    confirm = True


def main() -> int:
    try:
        if not os.path.exists(DATABASE_FILE):
            cc.Msg.warn(f"File '{DATABASE_FILE}' not found! Rebuilding...")
            if not cc.ckuser():
                cc.Msg.err("Permission error!")
                return 126

            cc.Msg.msg("-- cport init -- rebuild the database...")
            db = cc.CDatabase(cd.port("general/vagina"))

            db.create_initial_db()
            db.gen_all_ports()

            cc.Msg.msg("-- cport init -- regenerate the database...")
            cc.rebuild()

        if not os.path.exists(LOCAL_LIB_DIR):
            cc.Msg.warn(f"Directory '{LOCAL_LIB_DIR}' not found! Creating...")
            os.makedirs(LOCAL_LIB_DIR)

        if not os.path.isfile(DATABASE_USER_FILE):
            cc.Msg.warn(f"File '{DATABASE_USER_FILE}' not found! Rebuilding...")

            db = cc.CDatabase(cd.port("general/vagina"), dbfile=DATABASE_USER_FILE)
            db.create_initial_db()

        if not vars(args):
            parser.print_usage()
            return 1

        elif args.install:
            if not cc.ckuser():
                cc.Msg.err("Permission denied!")
                return 126
            return ccap.install(args.install, ignoredb, confirm, skip_checks)

        elif args.remove:
            if not cc.ckuser():
                cc.Msg.err("Permission denied!")
                return 126
            return ccap.remove(args.remove, ignoredb, confirm, skip_checks)

        elif args.info:
            return ccap.info(args.info)

        elif args.info_json:
            return ccap.info(args.info_json, json_enable=True)

        elif args.update:
            if not cc.ckuser():
                cc.Msg.err("Permission denied!")
                return 126
            return ccap.update(confirm)

        elif args.upgrade:
            return ccap.upgrade(args.upgrade)

        elif args.get_flist:
            return ccap.get_port_flist(args.get_flist)

        elif args.wipe_cache:
            if not cc.ckuser():
                cc.Msg.err("Permission denied!")
                return 126

            return ccap.wipe_cache()

        elif args.rebuild_db:
            if not cc.ckuser():
                cc.Msg.err("Permission denied!")
                return 126

            return ccap.rebuild_database()

        elif args.version:
            print(VER_MSG)
            return 0

        else:
            try:
                code = args.func(args)
                return code
            except AttributeError:
                parser.print_usage()
                return 1

    except KeyboardInterrupt:
        cc.Msg.err("Keyboard interrupt.")
        return 130

    except EOFError:
        cc.Msg.err("Keyboard interrupt.")
        return 130

    # except PermissionError:
    #     cc.Msg.err("Permission denied!")
    #     return 126


if __name__ == "__main__":
    sys.exit(main())
