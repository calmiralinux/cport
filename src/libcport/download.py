#!/usr/bin/python3
#
# download.py
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import shutil
import tempfile
import urllib.parse
import urllib.request

import libcport.core as cc


def fname_from_url(url: str) -> str | None:
    fname = os.path.basename(urllib.parse.urlparse(url).path)
    if len(fname.strip(" \n\t.")) == 0:
        return None
    return fname


def fname_from_headers(headers) -> str | None:
    if type(headers) == str:
        headers = headers.splitlines()
    if type(headers) == list:
        headers = dict([x.split(':', 1) for x in headers])

    cdisp = headers.get("Content-Disposition")
    if not cdisp:
        return None

    cdtype = cdisp.split(";")
    if len(cdtype) == 1:
        return None

    if cdtype[0].strip().lower() not in ('inline', 'attachment'):
        return None

    fnames = [x for x in cdtype[1:] if x.strip().startwith('filename=')]
    if len(fnames) > 1:
        return None

    name = fnames[0].split('=')[1].strip(' \t"')
    name = os.path.basename(name)

    if not name:
        return None
    return name


def detect_filename(url: str, tgt: str = None, headers: list = None) -> str:
    names = dict(tgt='', url='', headers='')
    if tgt:
        names['tgt'] = tgt or ''
    if url:
        names['url'] = fname_from_url(url) or ''
    if headers:
        names['headers'] = fname_from_headers(headers) or ''
    return names['tgt'] or names['headers'] or names['url']


def download(url: str, tgt: str = None) -> str:
    outdir = None
    if tgt and os.path.isdir(tgt):
        outdir = tgt

    # get filename from temp file in current dir.
    prefix = detect_filename(url, tgt)
    fd, tmpfile = tempfile.mkstemp(".tmp", prefix=prefix, dir=".")
    os.close(fd)
    os.unlink(tmpfile)

    binurl = list(urllib.parse.urlsplit(url))
    binurl[2] = urllib.parse.quote(binurl[2])
    binurl = urllib.parse.urlunsplit(binurl)

    tmpfile, headers = urllib.request.urlretrieve(binurl, tmpfile)
    fname = detect_filename(url, tgt, headers)

    cc.dbg_msg(f"{url=}\n{headers=}\n\n{fname=}\n{tmpfile=}")

    shutil.move(tmpfile, fname)

    return fname
