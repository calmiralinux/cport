#!/usr/bin/python3
#
# update.py
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import shutil
import tarfile

import libcport.constants as ccc
import libcport.core as cc
import toml
import wget

URL = ccc.PORTS_REPO_URL
BRANCH = ccc.UPDATE_BRANCH


class Check:

    def __init__(self, path: str):
        self.path = path

    def _get_metadata(self, mode: int = 1) -> dict:
        data = [
            ccc.PORTS_DIR,
            f"{self.path}/ports"
        ]
        return toml.load(f"{data[mode]}/metadata.toml")

    def _get_ports(self) -> tuple[list, list]:
        d0 = self._get_metadata(mode=0)
        d1 = self._get_metadata(mode=1)

        return d0['port_sys']['ports'], d1['port_sys']['ports']

    def _clean_sys(self) -> None:
        try:
            shutil.rmtree(f"{self.path}/ports")
            os.remove(f"{ccc.CACHE_DOWNLOADED_DIR}/ports.txz")
        except FileNotFoundError:
            pass

    def check_release(self) -> bool:
        rel = cc.get_calm_release()
        ports_rel = self._get_metadata(mode=1)['system']['release']

        return rel in ports_rel

    def get_added_ports(self) -> list[str]:
        ports_inst = self._get_ports()[0]
        ports_dwnl = self._get_ports()[1]
        changes = []

        for port in ports_dwnl:
            if port not in ports_inst:
                changes.append(port)

        return changes

    def get_updated_ports(self) -> list[dict]:
        ports_dwnl = self._get_ports()[1]
        changes = []

        for port in ports_dwnl:
            ignore_ports = self.get_added_ports()
            if port in ignore_ports:
                continue

            path_tmp = f"{self.path}/ports/{port}/port.toml"
            path_ins = f"{ccc.PORTS_DIR}/{port}/port.toml"

            cc.dbg_msg(f"checking port '{port}'...")

            data_tmp = toml.load(path_tmp)
            data_ins = toml.load(path_ins)

            version_tmp = data_tmp['package']['version']
            version_ins = data_ins['package']['version']

            if version_tmp != version_ins:
                data = {
                    'name': port,
                    'version': {
                        'old': version_ins,
                        'new': version_tmp
                    }
                }
                changes.append(data)

        return changes

    def get_removed_ports(self) -> list[str]:
        ports_inst = self._get_ports()[0]
        ports_dwnl = self._get_ports()[1]
        changes = []

        for port in ports_inst:
            if port not in ports_dwnl:
                changes.append(port)

        return changes

    def get_other_ports(self, file: str = "install") -> list[str]:
        ports_inst = self._get_ports()[0]
        changes = []

        for port in ports_inst:
            path_ins = f"{ccc.PORTS_DIR}/{port}"
            path_tmp = f"{self.path}/ports/{port}"

            if not os.path.exists(path_tmp):
                continue

            with open(f"{path_ins}/{file}") as f:
                inst_ins = f.read()

            with open(f"{path_tmp}/{file}") as f:
                inst_tmp = f.read()

            if inst_ins != inst_tmp:
                changes.append(port)

        return changes


class Update(Check):

    def __init__(self, path: str):
        super().__init__(path)
        self.path = path
        self.archive = f"{ccc.CACHE_DOWNLOADED_DIR}/ports.txz"
        self.url = f"{URL}/-/raw/{BRANCH}/ports.txz"

    def get(self) -> bool:
        if os.path.isfile(self.archive):
            os.remove(self.archive)

        wget.download(self.url, self.archive)
        print()

        return os.path.exists(self.archive)

    def unpack(self) -> bool:
        with tarfile.open(self.archive, 'r') as f:
            f.extractall(ccc.CACHE_UNPACKED_DIR)

        return os.path.isfile(
            f"{ccc.CACHE_UNPACKED_DIR}/ports"
        )

    def install(self) -> bool:
        if os.path.isdir(
                ccc.PORTS_DIR
        ) and os.path.isdir(self.path):
            shutil.rmtree(ccc.PORTS_DIR)
        else:
            raise FileNotFoundError

        shutil.copytree(self.path, ccc.PORTS_DIR)

        return os.path.isdir(ccc.PORTS_DIR)
