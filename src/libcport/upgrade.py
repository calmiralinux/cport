#!/usr/bin/python3
#
# upgrade.py - module for upgrading the ports
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import libcport.core as cc
import libcport.install as ci
import libcport.remove as cr
import libcport.datatypes as cd

import toml


def check_exist(port: cd.port) -> bool:
    """Checks if the port exists in the database"""
    db = cc.CDatabase(port)
    in_db: bool = db.check()
    db.close()

    return in_db


def compare_version(port: cd.port) -> bool:
    """Checks for a match between the port versions
    specified in the database and those specified
    in the ports system"""

    db = cc.CDatabase(port)

    info_db: list = db.get_one()
    info_toml: dict = toml.load(f"{port.path}/port.toml")

    ver_in_db: str = info_db[1]
    ver_in_toml: str = info_toml['package']['version']

    return ver_in_db == ver_in_toml


def upgrade_soft(port: cd.port) -> int:
    db = cc.CDatabase(port)
    if not check_exist(port):
        cc.Msg.warn(f"Port '{port.name}' isn't in database!")
        return 1

    if compare_version(port):
        cc.Msg.err(f"Nothing to do.")
        return 1

    db.remove()
    inst = ci.CInstall(port)

    inst.download()

    if not inst.cksum():
        return 1

    inst.unpack()
    r = inst.install()

    if r == 0:
        db.add()

    db.close()

    return r


def upgrade_hard(port: cd.port) -> int:
    db = cc.CDatabase(port)
    if not check_exist(port):
        cc.Msg.warn(f"Port '{port.name}' isn't in database!")
        return 1

    if compare_version(port):
        cc.Msg.err(f"Nothing to do.")
        return 1

    remove = cr.CRemove(port)
    remove.remove()

    db.remove()

    inst = ci.CInstall(port)

    inst.download()

    if not inst.cksum():
        return 1

    inst.unpack()
    r = inst.install()

    if r == 0:
        db.add()

    db.close()

    return r
