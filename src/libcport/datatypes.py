#!/usr/bin/python3
#
# datatypes.py - the 'port' data type for Python
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from dataclasses import dataclass
from dataclasses import field

from libcport.constants import PORTS_DIR
from libcport.exceptions import PortBrokenError
from libcport.exceptions import PortNotFoundError


@dataclass
class port:
    """
    Usage:
    name: libcport.datatypes.port

    Data:
      * name - port name;
      * path - path to port's files ('/usr/ports/...');
      * files - список файлов из path;
    """

    name: str
    path: str = field(init=False)
    files: list = field(init=False)

    def __post_init__(self):
        self.path = f"{PORTS_DIR}/{self.name}"

        if not os.path.isdir(self.path):
            raise PortNotFoundError

        self.is_found = True
        self.files = os.listdir(self.path)
        files = ('install', 'port.toml')  # TODO: in the stable version return file 'files.list'

        for file in files:
            if file not in self.files:
                raise PortBrokenError(file)
