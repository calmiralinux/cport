#!/usr/bin/python
#
# constants.py - base constants, variables and settings from the
# '/etc/cport.toml' file for cport
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import toml

home_dir = os.environ.get('HOME')

BASE_CONFIG = {
    "repo": {
        "url": "https://gitlab.com/calmiralinux/cabs/Ports",
        "branch": "testing"
    },
    "ports": {
        "path": "/usr/ports",
        "werror": "no"
    },
    "build_system": {
        "use_cache": "yes",
        "resolve_deps": "no"
    }
}

if not os.path.exists('/etc/cport.conf'):
    print("-- cport kernel -- create initial config...")
    try:
        with open('/etc/cport.conf', 'w') as f:
            toml.dump(BASE_CONFIG, f)
    except PermissionError:
        print("-- cport kernel -- Permission denied!")

data = toml.load('/etc/cport.conf')

# CONSTANTS
NAME: str = "cport"
VERSION: str = "1.0current_beta"
PORTS_DIR: str = data['ports'].get('path')
CACHE_DIR: str = "/var/cache/cport"
CACHE_DOWNLOADED_DIR: str = f"{CACHE_DIR}/downloads"
CACHE_UNPACKED_DIR: str = "/usr/src"
CONF_FILE: str = "/etc/cport.conf"
LIB_DIR: str = "/var/lib/Calmira"
LOCAL_LIB_DIR: str = f"{home_dir}/.local/share/Calmira"
DATABASE_FILE: str = f"{LIB_DIR}/software.db"
DATABASE_USER_FILE: str = f"{LOCAL_LIB_DIR}/software.db"
METADATA_FILE: str = f"{PORTS_DIR}/metadata.toml"
CALMIRA_RELEASE: str = "/etc/calm-release"
LOG_FILE: str = "/var/log/cport.log"
USER_LOG_FILE: str = f"{home_dir}/.cache/cport.log"

UPDATE_BRANCH: str = data['repo'].get('branch')
PORTS_REPO_URL: str = data['repo'].get('url')
WERROR: bool = data['ports'].get('werror')
USE_CACHE: str = data['build_system'].get('use_cache')
RESOLVE_DEPS: str = data['build_system'].get('resolve_deps')

PORT_TOML_PARAMS: dict = {
    "package": [
        "name", "version", "description", "maintainer",
        "releases", "priority", "usage", "upgrade_mode",
        "build_time", "description"
    ],
    "deps": [
        "required", "recommend", "optional"
    ],
    "port": [
        "url", "md5", "sha256", "file"
    ]
}
# Статусы секций: обязательная (True) и необязательная (False)
PORT_TOML_STATUSES: dict = {
    "package": True,
    "deps": False,
    "port": False
}
