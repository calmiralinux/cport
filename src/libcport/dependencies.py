#!/usr/bin/python3
#
# dependencies.py
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Any

import libcport.datatypes as cd
import libcport.core as cc
import libcport.exceptions as ce
import libcport.info as ci


class CDependencyGenerator:

    """Получает список всех зависимостей указанного порта"""

    def __init__(self, port: cd.port):
        self.port = port

        info = ci.CPortInfo(port)
        self.port_deps = info.deps()

        self.dependency_list = {}

    def _deps(self, port: cd.port = None) -> list[str]:
        if port is None:
            # Подразумевается, что в данном случае
            # используется имя искомого порта

            return self.port_deps.get('required')

        p_info = ci.CPortInfo(port)
        p_deps = p_info.deps()

        return p_deps.get('required')

    def deplist(self) -> dict:
        """Gets a list of all dependencies of the specified ports

        Return value:

        >>> a = CDependencyGenerator(cd.port('base/bash'))
        >>> a.deplist()
        {'base/bash': ['base/binutils',  'base/bison', 'base/coreutils', ...], 'base/binutils': ['base/coreutils',
        'base/diffutils', ...], 'base/bison': [...]}
        """

        deps = self._deps()
        if not deps:
            return {}

        for dep in deps:
            try:
                dependency_list = self._deps(cd.port(dep))
                if dependency_list is not None:
                    self.dependency_list[dep] = dependency_list
            except ce.PortNotFoundError:
                cc.dbg_msg(f"[CDependencyResolve.deplist] port '{dep}' not found")

        return self.dependency_list


class CDependencyResolver(CDependencyGenerator):

    """Форматирует список всех зависимостей порта"""

    def __init__(self, port: cd.port):
        super().__init__(port)
        self.dependency_form_list = []

    def _append(self, portname: str) -> None:
        """Adds ports to the list if all conditions are true:

         * The port is not yet in the list;
         * The port name to add is not the source port name;
         * The port to add is not in the 'installed' table of the cport database;"""

        try:
            db = cc.CDatabase(cd.port(portname))
        except ce.PortNotFoundError:
            return

        data = all([
            portname not in self.dependency_form_list,
            portname != self.port.name,
            not db.check()
        ])

        if data:
            self.dependency_form_list.append(portname)

    def gen_deplist(self) -> list:
        """Generates a list of dependencies of the specified ports

        Return value:

        >>> a = CDependencyResolver(cd.port('base/bash'))
        >>> a.gen_deplist()
        ['base/shadow', 'base/openssl', 'base/m4', 'base/perl', 'base/readline', 'base/ncurses', 'base/glibc']
        """

        deplist = self.deplist()

        for port in deplist.keys():
            for subport in deplist[port]:
                if subport not in self.dependency_form_list:
                    self._append(subport)
            self._append(port)

        return self.dependency_form_list[::-1]


def check_conflicts(port1: cd.port, port2: cd.port) -> dict[str, list[Any] | bool]:
    """Performs conflict checking between two ports. Return value:

    >>> check_conflicts(cd.port("base/acl"), cd.port("base/attr"))
    {
        "status": True,
        "conflict_files": ["/usr/bin/foo", "/usr/share/foo"]
    }

    If there are no conflicts, then 'status' = False, and the
    'conflict_files' list is empty. Otherwise, 'status' = True,
    and 'conflict_files' contains a list of conflicting files.
    """

    with open(f"{port1.path}/files.list") as f:
        port1_flist = f.read().splitlines()
    with open(f"{port2.path}/files.list") as f:
        port2_flist = f.read().splitlines()

    data = {
        "status": False,
        "conflict_files": []
    }

    for file in port1_flist:
        if file in port2_flist:
            cc.dbg_msg(f"Conflict: \n\t{port1.name} and {port2.name}:\n\t\t{file}")
            data['status'] = True
            data['conflict_files'].append(file)

    return data
