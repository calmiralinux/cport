#!/bin/bash -e
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

set +h
umask 022

CALM="/mnt/calm"
CALM_TGT=$(uname -m)-calm-linux-gnu
LC_ALL=POSIX
PATH=$CALM/tools/bin:$PATH
CONFIG_SITE=$CALM/usr/share/config.site

export CALM CALM_TGT LC_ALL PATH CONFIG_SITE

# Log messages
# Usage: log_msg [message] [status]
function log_msg() {
    # shellcheck disable=SC2046
    if [ $(whoami) != "root" ]; then
        log_file="$HOME/.cache/cport.log"
    else
        log_file="/var/log/cport.log"
    fi
    msg="[ `LC_ALL=C date` ] [ $2 ] [ from 'install' file ] $1"
    echo "$msg" >> $log_file
}

# Usage: $1 port
function configuration_log_msg() {
    log_msg "[Step 1] Port configuration" "info"
}

function make_log_msg() {
    log_msg "[Step 2] Performing a Port Assembly" "info"
}

function install_log_msg() {
    log_msg "[Step 3] Performing a port install" "info"
}

function setup_log_msg() {
    log_msg "[Step 4] Performing a port setup" "info"
}

# Usage:
# dialog_msg [default_no = 1 or 0]
function dialog_msg() {
    echo -e -n "\nContinue? "
    if [ $1 == "0" ]; then
        echo -n "[y/N] "
    else
        echo -n "[Y/n] "
    fi

    read run

    if [ $run == "n" ]; then
        return 1
    elif [ $run == "y" ]; then
        return 0
    else
        echo "Unknown input!"
        return dialog_msg
    fi
}

function msg() {
    echo -n -e "\e[1m==>\e[0m $1"
    log_msg $1 "info"
}

function err_msg() {
    echo -n -e "[\e[1;31m!\e[0m] $1"
    log_msg $1 "fail"
}

function warn_msg() {
    echo -n -e "[\e[1;33mW\e[0m] $1"
    log_msg $1 "warn"
}

function ok_msg() {
    echo -n -e "[\e[1;32m✓\e[0m] $1"
    if [ $2 == "0" ]; then
        log_msg $1 " ok "
    fi
}

function warn_header() {
    echo -e "\n\n\a\e[1;33m--==\e[0m \e[31mWARNING!\e[0m \e[1;33m==--\e[0m\n$1"
}

function err_header() {
    echo -e "\n\n\a\e[1;31m--==\e[0m \e[31mERROR!\e[0m \e[1;31m==--\e[0m\n$1"
}

function note_header() {
    echo -e "\n\n\a\e[1;32m--==\e[0m \e[1;33mNOTE!\e[0m \e[1;32m==--\e[0m\n$1"
}
