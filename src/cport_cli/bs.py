#!/usr/bin/python3
#
# bs.py - build system for Calmira GNU/Linux-libre
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

raise DeprecationWarning

import os
import shutil
import subprocess
import tarfile
import time

import libcport.constants as cC
import libcport.datatypes as cd
import toml
import wget
from cport_cli.shared import Msg

CACHE_DIR = "/mnt/calm/sources"
INFO_FILE = "/mnt/calm/build.toml"
PREPARATION_SCRIPTS_DIR = f"{cC.PORTS_DIR}/bs/scripts"
CREATE_DIRS = f"{PREPARATION_SCRIPTS_DIR}/dirs.sh"
CREATE_USER = f"{PREPARATION_SCRIPTS_DIR}/users.sh"

"""
Automated Building System for Calmira GNU/Linux-libre

Запускается от имени пользователя `calm'. Собирает порты для временного иструментания,
после входит в chroot и собирает порты core-редакции.
"""


class BuildingPreparations(object):
    """Carrying out preliminary preparations:

        - Creation of necessary files and directories for system building;
        - Checking the correctness of the host system for the building;
    """

    def __init__(self):
        if os.geteuid() != 0:
            raise PermissionError

    def _run(self, script: str) -> int:
        run = subprocess.run(script, shell=True)
        return run.returncode

    def create_limited_dirs_lyout(self) -> int:
        return self._run(CREATE_DIRS)

    def create_user(self) -> int:
        return self._run(CREATE_USER)


class BuildMachine(object):
    """Класс для сборки конкретного порта.
    
    Все необходимые порты [в нужном порядке] для сборки перечислены в файле
    '/usr/ports/bs/build_order.toml'. Там же содержатся сведения о системе,
    которую требуется собрать: её название, версия, номер билда и кодовое
    имя релиза.
    """

    def __init__(self, port: cd.port):
        self.port = port
        self.metadata = toml.load(f"{port.path}/port.toml")

    def _get_fname(self):
        url = self.metadata['port'].get('url')
        file = self.metadata['port'].get('file')

        if url is None:
            raise ValueError

        if file is not None:
            return file

        return wget.detect_filename(url)

    def _get_port_info(self) -> None:
        return

    def download(self):
        if self.metadata['port']['url'] == "none":
            return True

        file = f"{CACHE_DIR}/{self._get_fname()}"

        wget.download(self.metadata['port']['url'], CACHE_DIR)
        print()

        return os.path.isfile(file)

    def unpack(self) -> bool:
        if self.metadata['port']['url'] == "none":
            return True

        if self.metadata['port'].get('file') is not None:
            file = self.metadata['port']['file']
        else:
            file = f"{CACHE_DIR}/{self._get_fname()}"

        try:
            with tarfile.open(file, 'r') as f:
                f.extractall(path=CACHE_DIR)
        except tarfile.ReadError:
            Msg.err(f"Error reading file '{file}'!")
            return False
        except tarfile.CompressionError:
            Msg.err(f"File '{file}' compression error!")
            return False
        except tarfile.ExtractError:
            Msg.err(f"Error unpacking file '{file}'!")
            return False

        return True

    def install(self) -> int:
        script = f"{self.port.path}/install"
        run = subprocess.run(script, shell=True)

        return run.returncode


class BuildProgress(object):
    """A class for getting information about ports that have already been collected.

    Arguments:

        :arg port: str # Optional argument. If not specified, the
                       # 'check_builded' and 'set_builded' methods will not
                       # work.

    Methods:

        - check_builded() - checks if the specified port is built or not
        - set_builded() - sets the specified port as built
        - set_build_time() - sets the time of building all ports
        - get_builded_ports() - Gets a list of all built ports
    """

    def __init__(self, port=None):
        self.port = port
        self.data = toml.load(INFO_FILE)

        if not os.path.isfile(INFO_FILE):
            Msg.err(f"File '{INFO_FILE}' is not a file or not found")

            if os.path.isdir(INFO_FILE): shutil.rmtree(INFO_FILE)

        if not os.path.exists(INFO_FILE):
            Msg.err(f"File '{INFO_FILE}' not found in the filesystem!")
            Msg.msg("Create a base configuration in the '{INFO_FILE}'...")

            self._create_initial_conf()

    def _create_initial_conf(self) -> None:
        """Creates a base configuration file ('cport_cli.bs.INFO_FILE') if
        assigned."""

        data = {
            "ports": {
                "builded": [],
                "build_time": 0
            }
        }

        with open(INFO_FILE, 'w') as f:
            toml.dump(data, f)

    def _set_config(self) -> None:
        """This method writes the 'self.data' value to the INFO_FILE config file
        after doing something to change the config"""

        with open(INFO_FILE, 'w') as f:
            toml.dump(self.data, f)

    def check_builded(self) -> bool:
        if self.port is not None:
            return self.port in self.data['ports']['builded']
        else:
            raise ValueError

    def set_builded(self) -> None:
        self.data['ports']['builded'].append(self.port)
        self._set_config()

        # with open(INFO_FILE, 'w') as f:
        #     toml.dump(self.data, f)

    def set_build_time(self, tm: float) -> None:
        _tm = self.data['ports']['build_time']
        self.data['ports']['build_time'] = _tm + tm
        self._set_config()

        # with open(INFO_FILE, 'w') as f:
        #     toml.dump(self.data, f)

    def get_builded_ports(self) -> list[str]:
        return self.data['ports']['builded']


def build_pkgs(edition: str = "toolchain") -> int:
    md = toml.load(f"{cC.PORTS_DIR}/bs/build_order.toml")
    ports = md['editions'][edition]

    count = 1
    count_all = len(ports)

    Msg.msg("Start the build machine...")
    time_start_bs = time.time()

    for port in ports:
        progress = BuildProgress(port)
        if progress.check_builded():
            Msg.msg(f"Port '{port}' is already builded")
            continue

        Msg.msg(f"({count}/{count_all}) Install port '{port}'...")

        bs = BuildMachine(cd.port(port))
        time_start = time.time()

        Msg.msg("   Download port...")
        r = bs.download()

        if not r:
            return 1

        Msg.msg("   Unpack port...")
        r = bs.unpack()

        if not r:
            return 1

        Msg.msg("   Build port...")
        r = bs.install()

        time_end = time.time()
        Msg.msg(f"Build time (seconds): {time_end - time_start}")

        if r != 0:
            Msg.err(f"Port '{port}' returned a value other then 0! Rebuild it (1), break (2) or continue building (3)?")
            run = input()
            Msg.log_msg(f"   input: '{run}'")

            if run == "1":
                build_pkgs(edition)
            elif run == "3":
                progress.set_builded()
                continue
            elif run == "2":
                return r
            else:
                Msg.err("Unknown input!")
                return r

        count += 1
        progress.set_builded()

    time_end_bs = time.time()

    progress_t = BuildProgress()
    progress_t.set_build_time(time_end_bs - time_start_bs)

    return 0
