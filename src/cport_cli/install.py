#!/usr/bin/python3
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import tarfile
import zipfile
from urllib.error import URLError

import libcport.core as cc
import libcport.datatypes as cd
import libcport.install as ci


def install(port: cd.port, ignore_db: bool, skip_check: bool) -> int:
    db = cc.CDatabase(port)
    inst = ci.CInstall(port)

    if db.check() and not ignore_db:
        cc.Msg.warn(f"Port '{port.name}' already in the database!")
        cc.Msg.msg("Rebuilding...")

    try:
        if inst.check_fname():
            cc.Msg.msg(f"Download port '{port.name}'...")
            if inst.download():
                cc.Msg.ok("Download OK")
            else:
                cc.Msg.err("Download \033[1mERROR\033[0m")
                return 1

            cc.Msg.msg("Validating source file with md5sums...", endl=" ")
            if inst.cksum():
                print("Passed")
            else:
                cc.Msg.warn("NOT passed")
                run = cc.Msg.dialog()

                if not run:
                    cc.Msg.err("Aborted!")
                    return 1

            cc.Msg.msg(f"Extracting '{port.name}'...")
            inst.unpack()

        cc.Msg.msg(f"Building port '{port.name}'...")

        run = inst.install()

        if run != 0:
            cc.Msg.err(f"Build instructions was returned a value other then 0!")
            return run

        cc.Msg.ok("Build OK!")

        install_status = 0

        if not ignore_db:
            try:
                install_status = cc.check(port)
                cc.Msg.msg("Updating the database...", endl=" ")
                db.add(status=install_status)
                print("OK")
            except FileNotFoundError:
                cc.Msg.err(f"File '{port.path}/files.list' not found!")
                return 1

        if not skip_check:
            cc.Msg.msg(f"Checking the port '{port.name}' for correct installation...", endl=" ")
            if install_status == 0:
                cc.Msg.warn("Some port files were not found!")
            elif install_status == 3:
                cc.Msg.err("Port installation error!")
                run = 1
            else:
                cc.Msg.ok("Checking completed successfully")

        cc.Msg.msg("Clearing the cache...")
        fcount = cc.get_files_in_cache()
        cc.wipe_cache()
        cc.Msg.ok(f"{fcount} files was deleted!")

        return run
    except zipfile.BadZipFile:
        cc.Msg.err(f"Bad ZIP file '{inst.get_fname()}'!")
        return 1
    except tarfile.ReadError:
        cc.Msg.err(f"Error reading file '{inst.get_fname()}'!")
        return 1
    except tarfile.CompressionError:
        cc.Msg.err(f"File '{inst.get_fname()}' compression error!")
        return 1
    except tarfile.ExtractError:
        cc.Msg.err(f"Error unpacking file '{inst.get_fname()}'!")
        return 1
    except URLError:
        cc.Msg.err("Temporary failure in name resolution!")
        return 1
