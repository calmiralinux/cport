#!/usr/bin/python3
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import json
import toml

import libcport.constants as ccc
import libcport.exceptions as ce
import libcport.core as cc
import libcport.datatypes as cd
import libcport.info as ci
import libcport.dependencies as ccd


class CPortDependenciesCLI:

    def __init__(self, ports: list[cd.port]):
        self.ports: list[cd.port] = ports
        self.port_deps: list[cd.port] = []

    def _get_selected_ports(self) -> list[dict[str, str, bool]]:
        """Returns information about user-selected ports:

            [
                {
                    "name": "base/acl",
                    "version": "1.1",
                    "installed": True
                },
                {
                    "name": "base/attr",
                    "version": "1.2",
                    "installed": False
                }
                ...
            ]
        """

        ports: list[dict] = []

        for port in self.ports:
            try:
                _info = ci.CPortInfo(port)
                pkg: dict = _info.package()

                data = {
                    "name": pkg['name'],
                    "version": pkg['version'],
                    "installed": pkg['installed']
                }

                if port not in ports:
                    ports.append(data)
            except ce.PortNotFoundError as e:
                cc.Msg.err(f"Port '{e}' not found!")
                break

        return ports

    def _get_dependencies(self) -> list[dict[str, str]]:
        """Returns a list of all required software for installation:

            [
                {'name': 'base/m4', 'version': '1.4.19'},
                {'name': 'base/shadow', 'version': '4.12.3'},
                {'name': 'base/openssl', 'version': '3.0.5'},
                {'name': 'base/perl', 'version': '5.36.0'},
                {'name': 'base/readline', 'version': '8.1.2'},
                {'name': 'base/ncurses', 'version': '6.3'}
            ]"""

        ports: list[dict] = []
        ports_processed: list[str] = []

        for port in self.ports:
            try:
                cdr = ccd.CDependencyResolver(port)
                deplst = cdr.gen_deplist()

                for dep in deplst:
                    _info = ci.CPortInfo(cd.port(dep))
                    pkg = _info.package()

                    data = {
                        "name": dep,
                        "version": pkg['version']
                    }

                    condition = all([
                        data not in self.port_deps,
                        cd.port(data['name']) not in self.ports,
                        data['name'] not in ports_processed
                    ])

                    if condition:
                        self.port_deps.append(cd.port(dep))
                        ports.append(data)
                        ports_processed.append(data['name'])

            except ce.PortNotFoundError as e:
                cc.Msg.err(f"Port '{e}' not found!")
                break

        return ports

    def _calculate_usage(self) -> float:
        usage = 0.0
        for category in self.ports, self.port_deps:
            for port in category:
                _info = ci.CPortInfo(port)
                pkg = _info.package()

                usage += float(pkg['usage'])

        return usage

    def print_selected_ports(self) -> None:
        ports = self._get_selected_ports()

        print("\033[1mSelected ports:\033[0m")
        print("\t", end=" ")

        for port in ports:
            print(f"{port['name']}-{port['version']}", end=" ")
        print("\n")

    def print_deps(self) -> None:
        ports = self._get_dependencies()

        if len(ports) == 0:
            return

        print("\033[1mPorts that are not installed on the system:\033[0m")
        print("\t", end=" ")

        for port in ports:
            print(f"{port['name']}-{port['version']}", end=" ")
        print("\n")

    def print_usage(self) -> None:
        print("\033[1mHow much space will be taken up on the disk:\033[0m", end=" ")
        print(f"{self._calculate_usage()} Mb")


class CPortDependenciesJSON(CPortDependenciesCLI):

    def __init__(self, ports: list[cd.port]):
        super().__init__(ports)

    def print_selected_ports(self) -> None:
        print("\033[1mSelected ports:\033[0m")
        data: str = json.dumps(self._get_selected_ports(), indent=4)
        print(data)

    def print_deps(self) -> None:
        print("\033[1mPorts that are not installed on the system:\033[0m")
        data: str = json.dumps(self._get_dependencies(), indent=4)
        print(data)


class CPortInfoCLI:

    def __init__(self, ports: list[cd.port]):
        self.ports: list[cd.port] = ports

    def _data(self, port: cd.port, mode: str):
        info = ci.CPortInfo(port)
        modes = {
            "package": {
                "func": info.package,
                "msg": "Base information:"
            },
            "deps": {
                "func": info.deps,
                "msg": "\nDependencies:"
            }
        }
        return modes[mode]['func'], modes

    def _print(self, port: cd.port, mode: str):
        if port not in self.ports:
            return

        tp_str = ""
        func, modes = self._data(port, mode)

        if func() is None or len(func().keys()) == 0:
            return

        print(f"\033[1m{modes[mode]['msg']}")

        for i in func().keys():
            if type(func()[i]) == list:
                for index in func()[i]:
                    tp_str += str(index) + " "
            else:
                tp_str = func()[i]

            print("\033[1m{0:12}:\033[0m {1}".format(i, tp_str))
            tp_str = ""  # Clear string...

    def info(self):
        for port in self.ports:
            cc.Msg.header(f"Information about port '{port.name}'")

            for mode in "package", "deps":
                self._print(port, mode)

            print("\n")


class CPortInfoJSON(CPortInfoCLI):

    def __init__(self, ports: list[cd.port]):
        super().__init__(ports)

    def info(self):
        lst = []
        for port in self.ports:
            dt = {'name': port.name}

            for mode in "package", "deps":
                md, _ = self._data(port, mode)
                dt[mode] = md()
                lst.append(dt)

        print(json.dumps(lst, indent=4))
        # print(lst)


def is_installed(name: str) -> int:
    try:
        name = cd.port(name)
        db = cc.CDatabase(name)
    except ce.PortNotFoundError:
        cc.Msg.err(f"Port '{name}' not found!")
        return 1
    except ce.PortBrokenError:
        cc.Msg.err(f"Port '{name}' is broken!")
        return 1
    except toml.decoder.TomlDecodeError:
        cc.Msg.err(f"Error decoding the configuration file of the port '{name}'!")
        return 1

    print("{:15} :".format(name.name), end=" ")
    if db.check():
        print("installed")
        code = 0
    else:
        print("not installed")
        code = 1

    db.close()
    return code


def is_exist(port: str) -> bool:
    try:
        cd.port(port)
        print("{0:15}: {1}".format(port, "exist"))
        return True
    except ce.PortNotFoundError:
        print("{0:15}: {1}".format(port, "not exist"))
        return False
    except ce.PortBrokenError:
        cc.Msg.err(f"Port '{port}' is broken!")
        return False
    except toml.decoder.TomlDecodeError:
        cc.Msg.err(f"Error decoding the configuration file of the port '{port}'!")
        return False


def ports_check(args) -> int:
    """
        elif args.check_all:
            return check_deps()
        elif args.plint:
            return port_linter()
    """

    if args.check_exist:
        if is_exist(args.check_exist):
            return 0
        else:
            return 1
    elif args.check_installed:
        return is_installed(args.check_installed)
    else:
        data = toml.load(ccc.METADATA_FILE)

        releases = ""
        categories = ""

        for i in data['system']['release']:
            releases += str(i) + " "

        for i in data['port_sys']['categories']:
            categories += str(i) + " "

        print("\033[1mBase information about ports system:\033[0m")
        print("{0:16}: {1}".format('calmira releases', releases))
        print("{0:16}: {1}".format('categories', categories))

        return 0


"""
def json_info(name: port) -> None:
    conf = toml.load(f"{name.path}/port.toml")
    print(json.dumps(conf, ensure_ascii=False, indent=4))


def info(name: port) -> None:
    conf = toml.load(f"{name.path}/port.toml")
    package = [
        "name", "version", "maintainer",
        "releases", "priority", "usage",
        "upgrade_mode", "build_time",
        "description"
    ]
    deps = [
        "required", "recommend",
        "optional", "conflict"
    ]

    package_conf = conf.get('package')
    deps_conf = conf.get('deps')

    def form(params: list, data, header: str, prt: bool = False) -> None:
        tp_str = ""
        if data is None:
            return

        print(f"\033[1m{header}:\033[0m")
        for param in params:
            if data.get(param) is not None:
                print("{:15}:".format(param), end=" ")
                if type(data[param]) == list:
                    for i in data[param]:
                        if prt:
                            # TODO: добавить выделение портов жёлтым цветом в
                            # случае, если порт установлен в систему, однако в
                            # системе отсутствуют какие-либо файлы (статусы 2 и
                            # 3), либо версия в базе данных не соответствует
                            # версии в системе портов.
                            try:
                                # В том случае, если функция вызывается для
                                # показа зависимостей, то мы берём каждую из
                                # зависимостей и проверяем её наличие в базе
                                # данных. В том случае, если порт в базе
                                # присутствует, то выделить его зелёным цветом.
                                # Если порт в БД отсутствует, то не выделять
                                # никак. Если порт отсутствует в системе портов
                                # вообще, то выделить его красным цветом.
                                port_i = port(i)
                                db = CDatabase(port_i)
                                if db.check():
                                    i = f"\033[32m{i}\033[0m"
                                db.close()
                            except ce.PortNotFoundError:
                                i = f"\033[31m{i}\033[0m"
                        tp_str += str(i) + " "
                    print(tp_str)
                    tp_str = ""
                else:
                    print(data[param])

    cd = CDatabase(name)
    form(package, package_conf, "Base information")
    print("{0:15}: {1}".format("installed", cd.check()))
    form(deps, deps_conf, "\nDependencies", prt=True)
    cd.close()
"""


def port_linter() -> int:
    cl = cc.CList()
    code = 0

    ports = cl.all()

    for prt in ports:
        try:
            data = cc.p_lint(prt)

            if data:
                cc.Msg.ok(f"Port '{prt}': ok")
            else:
                cc.Msg.err(f"Port '{prt}': unknown error!")
                code = 1
        except ce.FilePortTomlError:
            cc.Msg.err(f"Port '{prt}': 'port.toml' file is broken or doesn't exist!")
        except ce.FileFilesListError:
            cc.Msg.err(f"Port '{prt}': 'files.list' file is broken or doesn't exist!")

    return code


def check_deps() -> int:
    cl = cc.CList()
    ports = cl.all()
    ports_broken = cl.broken()

    deps_types = ('required', 'recommend', 'optional', 'conflict')

    used_prts = []  # Уже проверенные порты
    errors = []  # Отсутствующие порты
    broken = 0  # Битые порты
    oks = []  # Присутствующие порты

    print("\033[3m\033[41m \033[0m - depends exists;", end=" ")
    print(f"\033[3m\033[42m \033[0m - depends does not exist\n{'-' * 80}")

    for name in ports:
        if name in ports_broken:
            cc.Msg.err(f"Port '{name}' is broken!")
            broken += 1
            continue

        name = cd.port(name)

        prt_data = toml.load(f"{name.path}/port.toml")
        prt_deps = prt_data['deps']

        for dep_type in deps_types:
            if prt_deps.get(dep_type) is None:
                # Пропуск в том случае, если у порта нет зависимостей
                continue
            deps = prt_deps[dep_type]

            for dep in deps:
                if dep in used_prts:
                    # Пропуск в том случае, если порт уже проверен - проверенные
                    # порты добавляются в список 'used_prts'
                    continue

                if dep in ports:
                    oks.append(dep)
                    used_prts.append(dep)
                else:
                    errors.append(dep)
                    used_prts.append(dep)

    if len(errors) > 0 or len(oks) > 0:
        used_prts.sort()
        for name in used_prts:
            if name in errors:
                print(f"\033[3m\033[41m \033[0m {name}")
            if name in oks:
                print(f"\033[3m\033[42m \033[0m {name}")

        print(f"Existing depends: \033[32m{len(oks)}\033[0m")
        print(f"Non-existing depends: \033[31m{len(errors)}\033[0m")
        print(f"Broken depends: \033[31m{broken}\033[0m")
        return 0
    else:
        return 0


def get_deps(name: cd.port) -> dict:
    """
    Returns a dictionary listing all ports that have the specified port as a
    dependency.

    Return value:

        {
            "required": [],
            "recommend": [],
            "optional": []
        }
    """

    cl = cc.CList()
    all_ports = cl.all()
    suggest = {
        "required": [],
        "recommend": [],
        "optional": []
    }
    dplst = ('required', 'recommend', 'optional')

    for prt in all_ports:
        prt_dt = cd.port(prt)
        data = toml.load(f"{prt_dt.path}/port.toml")
        deps = {}

        for dep in dplst:
            deps[dep] = data['deps'].get(dep)

        for dep in dplst:
            if deps[dep] is not None:
                if name.name in deps[dep]:
                    suggest[dep].append(prt)
    return suggest


def ports_check(args) -> int:
    # FIXME: функция не работает!!!
    if args.check_exist:
        return is_exist(args.check_exist)
    elif args.check_installed:
        return is_installed(args.check_installed)
    elif args.check_all:
        return check_deps()
    elif args.plint:
        return port_linter()
    else:
        cl = cc.CList()
        data = cl.all()

        releases = ""
        categories = ""

        for i in data['system']['release']:
            releases += str(i) + " "

        for i in data['port_sys']['categories']:
            categories += str(i) + " "

        print("\033[1mBase information about ports system:\033[0m")
        print("{0:16}: {1}".format('calmira releases', releases))
        print("{0:16}: {1}".format('categories', categories))

        return 0
