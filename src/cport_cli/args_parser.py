#!/usr/bin/python3
#
# args_parser.py
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""This module concatenates all other modules from `cport_shared`, creating
functions that will be used in `cport` when parsing command line arguments. The
module is a simple set of functions to abstract away the logic of the Calmira
GNU/Linux-libre distribution's port management utility.

Examples:
---------

>>> import cport_cli.args_parser as ccap # (Cport Cli Argument Parser - ccap)
>>> import argparse
>>> parser = argparse.ArgumentParser()
>>> parser.add_argument(
...     "--wipe-cache", "-W", action="store_true", dest="clean_cache",
...     help="Clean the cache"
... )
... 
>>> args = parser.parse_args()
>>> if args.clean_cache:
...     exit_value = ccap.wipe_cache()
...     exit(exit_value)
... 
"""
import json
import time

import cport_cli.info
import cport_cli.install
import cport_cli.remove
import cport_cli.update
import cport_cli.upgrade

import libcport.core as cc
import libcport.constants as ccc
import libcport.datatypes as cd
import libcport.exceptions as ce

import toml


def wipe_cache() -> int:
    r"""Function to delete all data in the cache

    Principle of operation
    ----------------------

         1. A list of all files in the cache is calculated (which is only needed
         to output to the console how many files have been deleted).
         2. If there are 0 files, then an error message is displayed: there is
         nothing to delete.
             - Returns 1
         3. If there are more than zero files, then all of them are deleted. A
         message is printed to `stdout` stating that they have all been removed.
             - Returns 0
    """

    files = cc.get_files_in_cache()
    cc.wipe_cache()
    if files > 0:
        cc.Msg.ok(f"{files} files have been deleted")
        return 0
    else:
        cc.Msg.err("Nothing to delete")
        return 1


def port_flist(prt: str) -> int:
    """Function to list port files to stdout

    Principle of operation
    ----------------------

      1. The `files.list` file of the port is opened and read.
      2. Its content is output to stdout.
         - Returns 0.
      3. If the `files.list` file or the port itself is missing, a message about
      this error is displayed in stderr.
         - Returns 1.
    """

    try:
        port = cd.port(prt)
        fl = f"{port.path}/files.list"
        with open(fl) as f:
            print(f.read())
        return 0
    except ce.PortNotFoundError:
        cc.Msg.err(f"Port '{prt}' not found!")
        return 1
    except FileNotFoundError:
        cc.Msg.err(f"File 'files.list' of port '{prt} not found")
        return 1


def install(ports: list[str], ign_db: bool = False, cnfrm: bool = True, skip_chk: bool = False) -> int:
    try:
        _info = cport_cli.info.CPortDependenciesCLI(list(map(cd.port, ports)))
    except ce.PortNotFoundError:
        cc.Msg.err(f"Some of ports not found!")
        return 1

    _info.print_selected_ports()
    _info.print_deps()
    _info.print_usage()

    if not cnfrm and not cc.Msg.dialog():
        return 1

    for port in ports:
        try:
            port = cd.port(port)

            cc.Msg.msg(f"Installing port '{port.name}'")

            # run = cport_cli.install(ignore_db=ign_db, confirm=cnfrm, skip_check=skip_chk)
            run = cport_cli.install.install(port, ign_db, skip_chk)

            if run != 0:
                return run
        except ce.PortNotFoundError:
            cc.Msg.err(f"Port '{port}' not found!")
            return 1
        except toml.decoder.TomlDecodeError:
            cc.Msg.err(f"Error decoding the configuration file of the port '{port}'")
            return 1
        # except ValueError:
        #    cc.Msg.err(f"File '{port.path}/port.toml' is broken!")
        #    return 1
    return 0


def remove(ports: list[str], ignore_db: bool = False, cnfrm: bool = False, skip_check: bool = False) -> int:
    for port in ports:
        try:
            port = cd.port(port)

            cc.Msg.header(f"Removing port '{port.name}'")
            run = cport_cli.remove.remove(port, ignore_db, cnfrm, skip_check)

            if run != 0:
                return run
        except ce.PortNotFoundError:
            cc.Msg.err(f"Port '{port}' not found!")
            return 1
        except toml.decoder.TomlDecodeError:
            cc.Msg.err(f"Error decoding the configuration file of the port '{port}'")
            return 1
    return 0


def info(ports: list[str], json_enable: bool = False) -> int:
    try:
        ports = list(map(cd.port, ports))

        if json_enable:
            inf = cport_cli.info.CPortInfoJSON(ports)
        else:
            inf = cport_cli.info.CPortInfoCLI(ports)

        inf.info()

        return 0
    except ce.PortNotFoundError:
        cc.Msg.err("Unknown port not found!")
        return 1


def is_installed(port: str) -> int:
    return cport_cli.info.is_installed(port)


def is_exist(port: str) -> int:
    return cport_cli.info.is_exist(port)


def ports_list(args) -> int:
    cl = cc.CList()
    # dplst = ('required', 'recommend', 'optional')

    pkg_list = cl.all()

    pkg_list_i = cl.installed()
    pkg_list_n = cl.not_installed()
    pkg_list_b = cl.broken()

    pkg_list_it = []
    pkg_list_nt = []
    pkg_list_bt = []

    for pkg in pkg_list_i:
        pkg_list_it.append(pkg[0])
    for pkg in pkg_list_n:
        pkg_list_nt.append(pkg)
    for pkg in pkg_list_b:
        pkg_list_bt.append(pkg)

    if args.list_json:
        json_enable = True
    else:
        json_enable = False

    def get_json(mode: str = "all") -> str:
        # dt = {}
        if mode == "all":
            dt = {
                "installed": [],
                "not_installed": [],
                "broken": [],
                "unknown_status": []
            }

            for _package in pkg_list:
                if _package in pkg_list_it:
                    dt['installed'].append(_package)
                elif _package in pkg_list_n:
                    dt['not_installed'].append(_package)
                elif _package in pkg_list_b:
                    dt['broken'].append(_package)
                else:
                    dt['unknown_status'].append(_package)

        elif mode == "installed":
            dt = {
                "installed": []
            }
            for _package in pkg_list_i:
                _dt = {
                    "name": _package[0],
                    "installation_date": _package[1]
                }
                dt['installed'].append(_dt)

        elif mode == "not_installed":
            dt = {
                "not_installed": []
            }
            for _package in pkg_list_n:
                dt['not_installed'].append(_package)

        elif mode == "broken":
            dt = {
                "broken": []
            }
            for _package in pkg_list_b:
                dt['broken'].append(_package)

        else:
            raise ValueError

        return json.dumps(dt, indent=4, ensure_ascii=False)

    if args.list_all:
        if json_enable:
            print(get_json("all"))
            return 0

        print("\033[3m\033[42m \033[0m - installed ports;", end=" ")
        print("\033[3m\033[47m \033[0m - not installed ports;", end=" ")
        print(f"\033[3m\033[41m \033[0m - broken ports\n{'-' * 80}")

        for package in pkg_list:
            if package in pkg_list_it:
                print(f"\033[3m\033[42m \033[0m {package}")
            elif package in pkg_list_nt:
                print(f"\033[3m\033[47m \033[0m {package}")
            elif package in pkg_list_bt:
                print(f"\033[3m\033[41m \033[0m {package} - \033[31mbroken\033[0m")

        return 0

    elif args.list_installed:
        if json_enable:
            print(get_json("installed"))
            return 0
        pkg_lst_all = cl.installed()

        print("\033[1m{0:30} {1}\n{2}\033[0m".format(
            "port name", "installation date", 55 * '-'
        ))

        for pkg in pkg_lst_all:
            tp_str = "{0:30} {1}".format(pkg[0], time.ctime(float(pkg[1])))
            print(tp_str)

        return 0

    elif args.list_not_installed:
        if json_enable:
            print(get_json("not_installed"))
            return 0
        pkg_list = pkg_list_n
    elif args.list_broken:
        if json_enable:
            print(get_json("broken"))
            return 0
        pkg_list = pkg_list_b

    else:
        if json_enable:
            print(get_json())
            return 0

    for package in pkg_list:
        print(package)

    return 0


def update(confirm: bool = False) -> int:
    dt = cport_cli.update.check()
    if dt == 1:
        return 1
    elif dt == -1:
        return 0

    if not confirm and not cc.Msg.dialog():
        return 1

    cc.Msg.msg("Updating the ports system...")
    u = cport_cli.update.Update(f"{ccc.CACHE_UNPACKED_DIR}/ports")
    if u.install():
        cc.Msg.ok("Update complete!")
        cc.Msg.msg("Update the database...")

        db = cc.CDatabase(cd.port("general/vagina"))
        db.gen_all_ports()
        db.close()

        return 0
    else:
        cc.Msg.err("Update FAIL!")
        return 1


def upgrade(args) -> int:
    return cport_cli.upgrade.main(args)


def rebuild_database() -> int:
    cc.rebuild()
    return 0


"""
    elif args.list_deps:
        if json_enable:
            print(get_json("deps"))
            return 0
        dps = []
        for dep in dplst:
            for pkg in pkg_list_d[dep]:
                dps.append(f"\033[1m{dep}:\033[0m\t{pkg}")
        pkg_list = dps
"""


def port_deps(args) -> int:
    def get_json(_mode: str = "all") -> str:
        """Work modes:

         * all [default];
         * required;
         * recommend;
         * optional
        """

        dt = {
            "port_name": prt.name
        }

        if _mode == "all":
            for _dep in dplst:
                dt[_dep] = pkg_lst.get(_dep)
        else:
            dt[_mode] = pkg_lst.get(_mode)

        return json.dumps(dt, indent=4, ensure_ascii=False)

    dplst = ccc.PORT_TOML_PARAMS['deps']

    if args.deps_json:
        json_enable = True
    else:
        json_enable = False

    prt_name = args.dest_name

    try:
        prt = cd.port(prt_name)
    except ce.PortNotFoundError:
        cc.Msg.err(f"Port '{prt_name}' not found!")
        return 1

    pkg_lst = cport_cli.info.get_deps(prt)

    if args.required_deps:
        mode = 'required'
    elif args.recommend_deps:
        mode = 'recommend'
    elif args.optional_deps:
        mode = 'optional'
    else:
        mode = 'all'

    if json_enable:
        print(get_json(mode))
        return 0

    if mode == "all":
        for mode in dplst:
            for dep in pkg_lst[mode]:
                print(f"\033[1m{mode}:\t\033[0m {dep}")
        return 0

    for dep in pkg_lst[mode]:
        print(dep)


def get_port_flist(port: str) -> int:
    try:
        port = cd.port(port)
        with open(f"{port.path}/files.list") as f:
            print(f.read())
        return 0
    except FileNotFoundError:
        cc.Msg.err(f"File '{port.path}/files.list' not found!")
        return 1
    except ce.PortNotFoundError:
        cc.Msg.err(f"Port '{port}' not found!")
        return 1
